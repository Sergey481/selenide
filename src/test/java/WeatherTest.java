import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class WeatherTest {

    @Test
    public void tempTest() {
        WeatherPage weatherPage = open("https://yandex.ru/", WeatherPage.class);

        assertNotEquals(8722, weatherPage.getTemp());
    }
}
