import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class WeatherPage {
    @FindBy(how = How.CSS, using = "#wd-_weather > div > div.widget__content.weather__content-outer > div > a > div.weather__temp")
    private SelenideElement temp;

    public int getTemp() {
        return (int) this.temp.getOwnText().charAt(0);
    }
}
